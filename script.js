var addX = true;
var addO = false;
var circleWon = false;
var crossWon = false;

$('.content__game-field').on('click', function() {
    var $this = $(this);
    var findX = $this.find('.cross');
    var findO = $this.find('.circle');

    if (addX && !addO && findO.length !== 1 && findX.length !== 1) {
        $(this).append('<span class="cross">X</span>');
        addX = false;
        addO = true;
        checkIfGameIsOver('.cross');
        robotPlayer();
    }});

function robotPlayer() {
    var randomRow = Math.floor(Math.random() * 3) + 1
    var randomColumn = Math.floor(Math.random() * 3) + 1
    var $element = $('.content__game-row').find('.field-' + randomRow + '-' + randomColumn);
    var check = $('.cross, .circle').length;

    if (addO && !addX && $element.find('.cross').length == 0 && $element.find('.circle').length == 0) {
        $element.append('<span class="circle">O</span>');
        addX = true;
        addO = false;
        checkIfGameIsOver('.circle');
    } else if (($element.find('.cross').length !== 1 || $element.find('.circle').length !== 1) && check < 9) {
        robotPlayer();
    }
}

function checkIfGameIsOver(element) {
    var check = $('.cross, .circle').length;
    var gameWon = false;
    var $gameBoard = $('.content__game');
    var $row11 = $gameBoard.find('.field-1-1');
    var $row12 = $gameBoard.find('.field-1-2');
    var $row13 = $gameBoard.find('.field-1-3');
    var $row21 = $gameBoard.find('.field-2-1');
    var $row22 = $gameBoard.find('.field-2-2');
    var $row23 = $gameBoard.find('.field-2-3');
    var $row31 = $gameBoard.find('.field-3-1');
    var $row32 = $gameBoard.find('.field-3-2');
    var $row33 = $gameBoard.find('.field-3-3');

    /* Logic for winning */

    setTimeout(function () {
        if ($row11.find(element).length == 1 && $row12.find(element).length == 1 && $row13.find(element).length == 1 ||
            $row11.find(element).length == 1 && $row21.find(element).length == 1 && $row31.find(element).length == 1 ||
            $row11.find(element).length == 1 && $row22.find(element).length == 1 && $row33.find(element).length == 1 ||
            $row13.find(element).length == 1 && $row22.find(element).length == 1 && $row31.find(element).length == 1 ||
            $row31.find(element).length == 1 && $row32.find(element).length == 1 && $row33.find(element).length == 1 ||
            $row13.find(element).length == 1 && $row23.find(element).length == 1 && $row33.find(element).length == 1 ||
            $row21.find(element).length == 1 && $row22.find(element).length == 1 && $row23.find(element).length == 1 ||
            $row12.find(element).length == 1 && $row22.find(element).length == 1 && $row32.find(element).length == 1) {
            gameWon = true;
                if (($(element).text().indexOf('X') != -1) && !($(element).text().indexOf('O') != -1) && !circleWon) {
                    element = 'Ristid';
                    crossWon = true;
                    alert(element + ' võitsid!' );
                    location.reload();
                    return;
                } else if (($(element).text().indexOf('O') != -1) && !($(element).text().indexOf('X') != -1) && !crossWon) {
                    element = 'Ringid';
                    circleWon = true;
                    alert(element + ' võitsid!' );
                    location.reload();
                    return;
            }
            return;
        }

        gameWon = false;
        circleWon = false;
        crossWon = false;

         if (check == 9 && !gameWon) {
           alert("Tekkis viik!");
           location.reload();
           return;
       }
    }, 75);
}
